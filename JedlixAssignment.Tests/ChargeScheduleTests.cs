using JedlixAssignment.Services;
using JedlixAssignment.Shared.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;

namespace JedlixAssignment.Tests;

public class Tests
{
    private readonly IChargingScheduler _scheduler = new ChargingScheduler();
    
    private ChargeScheduleRequest GetDefaultRequest() => new ChargeScheduleRequest {
            StartingTime = DateTime.Parse("4/8/2023 7:00:00 pm"),
            CarData = new CarData
            {
                BatteryCapacity = 55,
                ChargePower = 6.7m,
                CurrentBatteryLevel = 12
            },
            UserSettings = new UserSettings
            {
                DesiredStateOfCharge = 80,
                DirectChargingPercentage = 32,
                LeaveTime = TimeOnly.Parse("09:00"),
                Tariffs = new[]
                {
                    new Tariff {
                        StartTime = TimeOnly.Parse("00:00"),
                        EndTime = TimeOnly.Parse("07:30"),
                        EnergyPrice = 12m
                    },new Tariff {
                        StartTime = TimeOnly.Parse("07:30"),
                        EndTime = TimeOnly.Parse("14:30"),
                        EnergyPrice = 24.3m
                    },new Tariff {
                        StartTime = TimeOnly.Parse("14:30"),
                        EndTime = TimeOnly.Parse("20:30"),
                        EnergyPrice = 16.2m
                    },new Tariff {
                        StartTime = TimeOnly.Parse("20:30"),
                        EndTime = TimeOnly.Parse("23:59"),
                        EnergyPrice = 6.1m
                    }
                }
            }
        };
     
    [Test]
    public void SimpleRequestTest()
    {
        var request = GetDefaultRequest();
        var schedule = _scheduler.GetSchedule(request);
        Assert.IsNotEmpty(schedule);
        Assert.IsTrue(schedule.First().IsCharging);
        Assert.IsFalse(schedule.Last().IsCharging);
    }
    
    [Test]
    public void JsonRequestTest()
    {
        var jsonBody = File.ReadAllText("./TestRequest.json"); 
        var request = JsonConvert.DeserializeObject<ChargeScheduleRequest>(jsonBody);
        var schedule = _scheduler.GetSchedule(request);
        Assert.AreEqual(5, schedule.Length);
        Assert.IsTrue(schedule.All(x => x.IsCharging));
    }
    
    [Test]
    public void SingleTariffTest()
    {
        var request = GetDefaultRequest();
        request.UserSettings.Tariffs = new[] {
            new Tariff
            {
                StartTime = TimeOnly.Parse("00:00"),
                EndTime = TimeOnly.Parse("23:59"),
                EnergyPrice = 6.1m
            }
        };

        var schedule = _scheduler.GetSchedule(request);
        Assert.IsNotEmpty(schedule);
        Assert.IsTrue(schedule.First().IsCharging);
        Assert.IsFalse(schedule.Last().IsCharging);
    }
    
    [Test]
    public void DirectChargingDurationTest()
    {
        var request = GetDefaultRequest();
        request.UserSettings.DirectChargingPercentage = 10;
        request.CarData.BatteryCapacity = 100;
        request.CarData.CurrentBatteryLevel = 0;
        request.CarData.ChargePower = 5m;
         
        request.UserSettings.Tariffs = new[] {
            new Tariff
            {
                StartTime = TimeOnly.Parse("00:00"),
                EndTime = TimeOnly.Parse("23:59"),
                EnergyPrice = 10m
            }
        };

        var schedule = _scheduler.GetSchedule(request);
        Assert.IsNotEmpty(schedule);
        Assert.IsTrue(schedule.First().IsCharging);
        Assert.AreEqual(request.StartingTime.AddHours(2), schedule[0].EndTime);
    }
    
    
    [Test]
    public void DirectChargingSatisfiedTest()
    {
        var request = GetDefaultRequest();
        request.UserSettings.DirectChargingPercentage = 10;
        request.CarData.BatteryCapacity = 50;
        request.CarData.CurrentBatteryLevel = 20;
        request.CarData.ChargePower = 5m;
         
        request.UserSettings.Tariffs = new[] {
            new Tariff
            {
                StartTime = TimeOnly.Parse("00:00"),
                EndTime = TimeOnly.Parse("23:59"),
                EnergyPrice = 10m
            }
        };

        var schedule = _scheduler.GetSchedule(request);
        Assert.IsNotEmpty(schedule);
        Assert.IsTrue(schedule.First().IsCharging);
        Assert.IsFalse(schedule.Last().IsCharging);
        Assert.AreEqual(DateTime.Parse("5/8/2023 2:00:00 am"), schedule.Last().StartTime);
    }
    
    [Test]
    public void DirectChargingIntoLeaveTimeTest()
    {
        var request = GetDefaultRequest(); 
        request.UserSettings.LeaveTime = TimeOnly.FromDateTime(request.StartingTime.AddHours(1)); 
        request.UserSettings.DirectChargingPercentage = 10;
        request.CarData.BatteryCapacity = 100;
        request.CarData.CurrentBatteryLevel = 0;
        request.CarData.ChargePower = 5m;
        request.UserSettings.Tariffs = new[] {
            new Tariff
            {
                StartTime = TimeOnly.Parse("00:00"),
                EndTime = TimeOnly.Parse("23:59"),
                EnergyPrice = 12m
            }
        };

        // Expecting 1 hour charge, when 2 hours is required by direct charge
        var schedule = _scheduler.GetSchedule(request);
        Assert.AreEqual(1, schedule.Length);
        Assert.AreEqual(1, schedule[0].Duration.TotalHours);
    }
    
    [Test]
    public void MultipleTariffDateOrderTest()
    {
        var request = GetDefaultRequest();
        request.CarData.BatteryCapacity = 100;
        request.UserSettings.DirectChargingPercentage = 0;
        request.CarData.CurrentBatteryLevel = 0;
        request.CarData.ChargePower = 5m;
        request.StartingTime = DateTime.Parse("4/8/2023 7:00:00 am");
        request.UserSettings.LeaveTime = TimeOnly.Parse("16:30");
        request.UserSettings.Tariffs = new[]
        {
            new Tariff { StartTime = TimeOnly.Parse("00:00"), EndTime = TimeOnly.Parse("07:30"), EnergyPrice = 12m },
            new Tariff { StartTime = TimeOnly.Parse("07:30"), EndTime = TimeOnly.Parse("08:01"), EnergyPrice = 10m },
            new Tariff { StartTime = TimeOnly.Parse("08:01"), EndTime = TimeOnly.Parse("08:32"), EnergyPrice = 8m },
            new Tariff { StartTime = TimeOnly.Parse("08:32"), EndTime = TimeOnly.Parse("09:03"), EnergyPrice = 6m },
            new Tariff { StartTime = TimeOnly.Parse("09:03"), EndTime = TimeOnly.Parse("23:59"), EnergyPrice = 12m },
        };
            
        // TODO Ending time of the last schedule doesn't line up with the LeaveTime
        var schedule = _scheduler.GetSchedule(request);
        Assert.IsTrue(schedule.Length == 5);
        Assert.AreEqual(TimeOnly.FromDateTime(request.StartingTime), TimeOnly.FromDateTime(schedule[0].StartTime));
        Assert.AreEqual(request.UserSettings.Tariffs[1].StartTime, TimeOnly.FromDateTime(schedule[1].StartTime));
        Assert.AreEqual(request.UserSettings.Tariffs[2].StartTime, TimeOnly.FromDateTime(schedule[2].StartTime));
        Assert.AreEqual(request.UserSettings.Tariffs[3].StartTime, TimeOnly.FromDateTime(schedule[3].StartTime));
        Assert.AreEqual(request.UserSettings.Tariffs[4].StartTime, TimeOnly.FromDateTime(schedule[4].StartTime)); 
    }
    
    [Test]
    public void MultipleTariffWithGapTest()
    {
        var request = GetDefaultRequest();
        request.UserSettings.LeaveTime = TimeOnly.Parse("16:30");
        request.UserSettings.Tariffs = new[]
        {
            new Tariff
            {
                StartTime = TimeOnly.Parse("00:00"),
                EndTime = TimeOnly.Parse("07:30"),
                EnergyPrice = 12m
            },
            new Tariff
            {
                StartTime = TimeOnly.Parse("14:30"),
                EndTime = TimeOnly.Parse("20:30"),
                EnergyPrice = 16.2m
            },
            new Tariff
            {
                StartTime = TimeOnly.Parse("20:30"),
                EndTime = TimeOnly.Parse("23:59"),
                EnergyPrice = 6.1m
            }
        };
             
        // This test fails because the timeCursor in the opportunity generator, doesn't consider
        // the jump in time between Tariff when a gap is present. This should fail validation
        Assert.Throws<Exception>(() => _scheduler.GetSchedule(request));
    }
    
    
    [Test]
    public void MultipleTariffWithMissingEndTariffTest()
    {
        var request = GetDefaultRequest();
        request.UserSettings.LeaveTime = TimeOnly.Parse("9:30");
        request.UserSettings.Tariffs = new[]
        {
            new Tariff
            {
                StartTime = TimeOnly.Parse("00:00"),
                EndTime = TimeOnly.Parse("07:30"),
                EnergyPrice = 12m
            },
            new Tariff
            {
                StartTime = TimeOnly.Parse("14:30"),
                EndTime = TimeOnly.Parse("20:30"),
                EnergyPrice = 16.2m
            },
            new Tariff
            {
                StartTime = TimeOnly.Parse("20:30"),
                EndTime = TimeOnly.Parse("23:59"),
                EnergyPrice = 6.1m
            }
        };

        // Fails because leave time doesn't have a tariff
        // How to handle this isn't defined, so we'll expect an error.
        Assert.Throws<Exception>(() => _scheduler.GetSchedule(request));
    }

    [Test]
    public void SingleTariffCrossDayTest()
    {
        var request = GetDefaultRequest();
        request.StartingTime = DateTime.Now;
        request.UserSettings.LeaveTime = TimeOnly.FromDateTime(DateTime.Now.AddDays(0.9));
        request.UserSettings.Tariffs = new[] {
            new Tariff
            {
                StartTime = TimeOnly.Parse("00:00"),
                EndTime = TimeOnly.Parse("23:59"),
                EnergyPrice = 12m
            }
        };

        // Fails because leave time doesn't have a tariff  
        var schedule = _scheduler.GetSchedule(request);
        Assert.IsNotEmpty(schedule);
        Assert.IsTrue(schedule.First().IsCharging);
        Assert.IsFalse(schedule.Last().IsCharging);
    }
    
    [Test]
    public void SingleTariffIdenticalLeaveTimeNoDirectTest()
    {
        var request = GetDefaultRequest();
        request.StartingTime = DateTime.Now;
        request.UserSettings.DirectChargingPercentage = 0;
        request.UserSettings.LeaveTime = TimeOnly.FromDateTime(request.StartingTime.AddDays(1));
        
        // There is no sane way to determine if a request is made at LeaveTime, we could assume that requests
        // made with a leave time of Now would mean tomorrow
        var schedule = _scheduler.GetSchedule(request);
        Assert.IsEmpty(schedule);
    } 
}