using System.Text.Json.Serialization;
using JedlixAssignment.Shared.Converters;

namespace JedlixAssignment.Shared.Models;

public class Tariff {
    [JsonConverter(typeof(TimeOnlyJsonConverter))]
    public TimeOnly StartTime { get; set; }
    [JsonConverter(typeof(TimeOnlyJsonConverter))]
    public TimeOnly EndTime { get; set; }
    public decimal EnergyPrice { get; set; } 
    public TimeSpan Duration { get => EndTime - StartTime; }
}