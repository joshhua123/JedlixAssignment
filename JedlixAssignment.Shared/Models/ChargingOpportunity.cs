namespace JedlixAssignment.Shared.Models;

public class ChargingOpportunity
{
    public ChargingOpportunity(Tariff tariff, DateTime startingTime, DateTime endingTime)
    {
        Start = startingTime;
        End = endingTime;
        Cost = tariff.EnergyPrice;
        Tariff = tariff;
    }
    public TimeSpan Duration => End - Start;
    public DateTime Start { get; set;}
    public DateTime End { get; set; }
    public decimal Cost { get; set; } 
    public decimal Utilization { get; set; }
    public Tariff Tariff { get; set; }
}