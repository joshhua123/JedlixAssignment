using System.Text.Json.Serialization;
using JedlixAssignment.Shared.Converters;

namespace JedlixAssignment.Shared.Models;

public class UserSettings
{
    public int DesiredStateOfCharge { get; set; }
    [JsonConverter(typeof(TimeOnlyJsonConverter))]
    public TimeOnly LeaveTime { get; set; }
    public int DirectChargingPercentage { get; set; }
    public required Tariff[] Tariffs { get; set; }
}