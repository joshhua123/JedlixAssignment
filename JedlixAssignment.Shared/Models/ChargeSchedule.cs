using System.Text.Json.Serialization;

namespace JedlixAssignment.Shared.Models;

public class ChargeSchedule
{
    public DateTime StartTime { get; set; }
    public DateTime EndTime { get; set; }
    public bool IsCharging { get; set; }
    [JsonIgnore]
    public TimeSpan Duration => EndTime - StartTime;
}