namespace JedlixAssignment.Shared.Models;

public class ChargeScheduleRequest
{
    public DateTime StartingTime { get; set; }
    public required UserSettings UserSettings { get; set; }
    public required CarData CarData { get; set; }
}