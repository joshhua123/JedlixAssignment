using System.Globalization;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace JedlixAssignment.Shared.Converters;

/// <summary>
/// The JsonSerilization used by [FromBody] in the API controller endpoint isn't able to handle TimeOnly data types
/// to work around this we add expand on the serilizer to handle these data types in the format expected
/// </summary>
public class TimeOnlyJsonConverter : JsonConverter<TimeOnly>
{
    public override TimeOnly Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) =>
        TimeOnly.Parse(reader.GetString()!, CultureInfo.InvariantCulture);

    public override void Write(Utf8JsonWriter writer, TimeOnly dateTimeValue, JsonSerializerOptions options) =>
        writer.WriteStringValue(dateTimeValue.ToString());
}