using System.Text.Json.Serialization;
using JedlixAssignment.Services;
using JedlixAssignment.Shared.Models;
using Microsoft.AspNetCore.Mvc;

namespace JedlixAssignment.Controllers;

[ApiController]
[Route("[controller]")]
public class ChargingScheduleController
{
    private readonly IChargingScheduler _scheduler;
    
    public ChargingScheduleController(IChargingScheduler scheduler) {
        _scheduler = scheduler;
    }
    
    [HttpPost]
    public JsonResult Get([FromBody] ChargeScheduleRequest request) {
        return new JsonResult(_scheduler.GetSchedule(request));
    }
}