using JedlixAssignment.Shared.Models;

namespace JedlixAssignment.Services;

public interface IChargingScheduler
{
    ChargeSchedule[] GetSchedule(ChargeScheduleRequest request);

}