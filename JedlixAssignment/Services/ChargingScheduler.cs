using JedlixAssignment.Shared.Models;

namespace JedlixAssignment.Services;

public class ChargingScheduler : IChargingScheduler
{ 
    /// <summary>
    /// Get the tariff active during the sample time
    /// </summary>
    /// <param name="tariffs"></param>
    /// <param name="sample"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    private Tariff GetTariff(IEnumerable<Tariff> tariffs, TimeOnly sample) =>
        tariffs.FirstOrDefault(x => sample.IsBetween(x.StartTime, x.EndTime)) ?? throw new Exception();
      
    /// <summary>
    /// Get the next available tariff from the previous tariff, this approach isn't sensitive to gaps in tariff timings
    /// </summary>
    /// <param name="tariffs"></param>
    /// <param name="previous"></param>
    /// <returns></returns>
    private Tariff GetNextOpportunity(IEnumerable<Tariff> tariffs, Tariff previous) => 
        tariffs.MinBy(x => x.StartTime - previous.EndTime) ?? throw new Exception();  

    /// <summary> 
    /// Return a list of charging opportunities between the start and end time, considering cross-day changes
    /// </summary>
    /// <param name="user"></param>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <returns></returns>
    private IEnumerable<ChargingOpportunity> GetChargingOpportunities(UserSettings user, DateTime start, DateTime end)
    {
        if(start == end)
            yield break;
        
        var startTime = TimeOnly.FromDateTime(start);
        var endTime = TimeOnly.FromDateTime(end);
        var firstOpportunity = GetTariff(user.Tariffs, startTime);
        var lastOpportunity = GetTariff(user.Tariffs, endTime);
        var next = firstOpportunity;
        // Use the time cursor to track the current time an opportunity is starting and ending
        var timeCursor = start.Add(firstOpportunity.EndTime - startTime);

        if (timeCursor > end) {
            yield return new ChargingOpportunity(firstOpportunity, start, end);
            yield break;
        }
        // First opportunity of the sequence has a unique start time
        yield return new ChargingOpportunity(firstOpportunity, start, timeCursor);
        next = GetNextOpportunity(user.Tariffs, next);
        
        // Until we reach the end time, 
        var prevCursor = timeCursor;
        while ((timeCursor = timeCursor.Add(next.Duration)) < end)
        {
            yield return new ChargingOpportunity(next, prevCursor, timeCursor);
            next = GetNextOpportunity(user.Tariffs, next);
            prevCursor = timeCursor;
        }

        // Return the last opportunity terminated at the endTime. 
        yield return new ChargingOpportunity(lastOpportunity, prevCursor, end);
    }

    private TimeSpan CalculateDirectChargeDuration(UserSettings user, CarData car) {
        var currentChargePercentage = car.CurrentBatteryLevel / car.BatteryCapacity;
        var directChargingPercentage = user.DirectChargingPercentage / 100m;
        var deltaPercentage = directChargingPercentage - currentChargePercentage;
        var deltaPower = deltaPercentage * car.BatteryCapacity;
        var hours = Math.Max(0m, deltaPower / car.ChargePower);
        return TimeSpan.FromHours(Convert.ToDouble(hours));
    }

    /// <summary>
    /// Using a charging opportunities utilization, create a charging schedule
    /// This function could be used to move charging times within an opportunity to smooth out demand spikes
    /// At this stage we'll always charge at the start of an opportunity, 
    /// </summary>
    /// <param name="opportunities"></param>
    /// <param name="startTime"></param>
    /// <returns></returns>
    private IEnumerable<ChargeSchedule> GetChargingSchedule(IEnumerable<ChargingOpportunity> opportunities) =>
        opportunities.SelectMany(opportunity =>
        {
            if (opportunity.Utilization == 1m || opportunity.Utilization == 0m) { 
                return new[] { new ChargeSchedule {
                    StartTime = opportunity.Start, EndTime = opportunity.End, IsCharging = opportunity.Utilization != 0m
                }};
            }

            var durationOfFirstCharge = opportunity.Duration.TotalHours * (double) opportunity.Utilization;
            var endFirstOp = opportunity.Start.AddHours(durationOfFirstCharge);

            return new[]
            {
                new ChargeSchedule {StartTime = opportunity.Start, EndTime = endFirstOp, IsCharging = true},
                new ChargeSchedule {StartTime = endFirstOp, EndTime = opportunity.End, IsCharging = false}
            };
        });
    
    public ChargeSchedule[] GetSchedule(ChargeScheduleRequest request)
    {
        // First the car charges to the DirectChargingPercentage without considering cost
        // Then plan the optimal charging strategy using the known Tariffs
        // Create a set of possible charging opportunities between the expected
        //  time to be at the direct charging and the leavingTime.
        // Allocate % of utilization of a charging opportunity per entry in the set
        //  utilizing the lowest cost entry first
        var startTime = TimeOnly.FromDateTime(request.StartingTime);
        var chargingOpportunityDuration = request.UserSettings.LeaveTime - startTime;

        var directChargingDuration = CalculateDirectChargeDuration(request.UserSettings, request.CarData);
        directChargingDuration = directChargingDuration > chargingOpportunityDuration
            ? chargingOpportunityDuration
            : directChargingDuration;
        
        var startOptimalChargingAt = request.StartingTime.Add(directChargingDuration);
        var stopChargingAt = request.StartingTime.Add(request.UserSettings.LeaveTime - TimeOnly.FromDateTime(request.StartingTime));

        // Get all the charging opportunities considering the start time and a maximum charge rate, assuming we use
        // all available opportunities with a maximum charge rate, we can just sum the cost for the direct charging cost
        var directOpportunities =
            GetChargingOpportunities(request.UserSettings, request.StartingTime, startOptimalChargingAt);
        
        // Now we've considered the direct charging, we need to get the charging opportunities and allocate the utilization
        var optimalOpportunities =
            GetChargingOpportunities(request.UserSettings, startOptimalChargingAt, stopChargingAt);
        
        // Assuming we're at the direct charging percentage, how much more energy until we achieve the target
        var targetEnergyDelta = 
                                ((request.UserSettings.DesiredStateOfCharge - request.UserSettings.DirectChargingPercentage) / 100m) 
                                * request.CarData.BatteryCapacity;

        // Allocate utilization of opportunities based on the remaining targetEnergyDelta
        // and how much energy an opportunity will provide
        var chargingOpportunities = directOpportunities
            .Select(x =>
            {
                x.Utilization = 1m;
                return x;
            }).Concat(optimalOpportunities
                .OrderBy(x => x.Cost)
                .Select(opportunity => {
                    var energy = Convert.ToDecimal(opportunity.Duration.TotalHours) * request.CarData.ChargePower;
                    if (energy == 0m || targetEnergyDelta <= 0.001m)
                        return opportunity;
                    opportunity.Utilization = Math.Min(1m, targetEnergyDelta / energy);
                    targetEnergyDelta -= energy * opportunity.Utilization;
                    return opportunity;
                }));

        return GetChargingSchedule(chargingOpportunities).OrderBy(x => x.StartTime).ToArray();
    }
}